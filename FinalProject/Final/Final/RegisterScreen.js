mport React, {Component} from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	Image, 
	TouchableOpacity,
	FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
	render() {
	return(
		<View style={styles.container}>
			<View style={styles.navBar}>
				<Image source={require('./images/logo1.png')} style={{width: 490, height: 150}} />
			</View>	

				<Text style={styles.tabLogin}>R.E.G.I.S.T.E.R</Text>

				<View style={styles.tabUsers}>
					<Text style={styles.tabUser}>Username / email</Text>
					<input type="text" name="text" size= "30" />
					

					<Text style={styles.tabUser}>Password</Text>
					<input type="text" name="text" size= "30" />

					<Text style={styles.tabUser}>Ulangi password</Text>
					<input type="text" name="text" size= "30" />

					<View style={styles.tabDaftar}>
						<input type="button" onclick="kirim()" value="Daftar" size= "10" />
					</View>

						<Text style={styles.tabAtau}>atau</Text>

					<View style={styles.tabDaftar}>
						<input type="button" onclick="kirim()" value="Masuk" size= "10" />
					</View>
				</View>

		</View>

		
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	navBar: {
		height: 55,
		backgroundColor: 'white',
		elevation: 3,
		paddingHorizontal: 15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginLeft: 400,
		marginTop: 100
	},
	rightNav: {
		flexDirection: 'row'
	},
	navItem: {
		marginLeft: 25
	},
	tabLogin: {
		fontSize: 23,
		color: 'black',
		padding: 4,
		textAlign: 'center',
		paddingTop: 85
	},
	tabUser: {
		fontSize: 13,
		color: 'black',
		padding: 4,
		textAlign: 'center',
		paddingTop: 25,
		marginRight: 150,
		alignItems: 'center',
		justifyContent: 'center'
	},
	tabUsers: {
		fontSize: 13,
		color: 'black',
		padding: 4,
		textAlign: 'center',
		paddingTop: 25,
		marginRight: 25,
		alignItems: 'center',
		justifyContent: 'center'
	},
	tabDaftar: {
		paddingTop: 23
	}, 
	tabAtau: {
		marginRight: 5,
		paddingTop: 11
	}
});